<?php get_template_part( 'templates/modules/post-archive-banner' ); ?>

<div id="post-container" class="content-wrap">
  <div class="row">

    <div class="col-4 column--twitter">
      <div class="twitter-widget-header">
        <img width="72" height="72" src="<?php echo get_stylesheet_directory_uri(); ?>/dist/images/social-twitter.svg" alt="Twitter">
      </div>
      <?php echo do_shortcode("[fts_twitter twitter_name=lifebiosciences tweets_count=2 cover_photo=no stats_bar=no show_retweets=no show_replies=no]"); ?>
      <?php echo do_shortcode("[fts_twitter twitter_name=davidasinclair tweets_count=2 cover_photo=no stats_bar=no show_retweets=no show_replies=no]"); ?>
    </div>

    <div class="col-8">
      <?php if (!have_posts()) : ?>
        <div class="alert alert-warning">
          <?php _e('Sorry, no results were found.', 'sage'); ?>
        </div>
        <?php get_search_form(); ?>
      <?php endif; ?>

      <div class="posts-wrap">
        <?php while (have_posts()) : the_post(); ?>
          <?php get_template_part('templates/content', get_post_type() != 'post' ? get_post_type() : get_post_format()); ?>
        <?php endwhile; ?>
      </div>

      <?php the_posts_navigation(); ?>
    </div>
  </div>


</div>
