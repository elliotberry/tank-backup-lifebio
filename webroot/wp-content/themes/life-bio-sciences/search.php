<?php get_template_part('templates/page', 'header'); ?>

<div class="content-wrap">
  <?php if (!have_posts()) : ?>
    <div class="no-results">
      <div class="alert alert-warning">
        <?php _e('Sorry, no results were found.', 'sage'); ?>
      </div>
      <?php get_search_form(); ?>
    </div>
  <?php endif; ?>

  <div class="posts-wrap">
    <?php while (have_posts()) : the_post(); ?>
      <?php get_template_part('templates/content', 'search'); ?>
    <?php endwhile; ?>
  </div>

  <?php the_posts_navigation(); ?>
</div>

