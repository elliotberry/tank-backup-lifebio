<?php
/**
 * Variant of "Focus Area List" that should only appear on the Home Page.
 * Also, doesn't grab the focus area paragraphs.
 * Many of the styles are borrowed from .focus-area-list
 */
 ?>

<section class="focus-area-list focus-area-list--icons-only">
  <div class="container">
    <?php
      $title       = get_field('focus_areas_title');
      $text        = get_field('focus_areas_text');
      $button_text = get_field('focus_areas_button_text');
      $button_link = get_field('focus_areas_button_link');

      // text
      if ( $title ) {
        echo '<h2 class="section-title">' . $title . '</h2>';
      }
      if ( $text ) { ?>
        <div class="section-text">
          <?php echo $text; ?>
        </div>
      <?php }

      // focus areas
      $exclude_ids = get_field( 'exclude_focus_areas', 'option' );
      $args = array(
        'posts_per_page'   => -1,
        'post_type'    => 'lifebio_focusarea',
        'post_status'  => 'publish',
        'post__not_in' => $exclude_ids, // exclude animal companions
        'order'        => 'ASC',
        'orderby'      => 'menu_order',
      );
      $query = new WP_Query( $args );

      echo '<ul class="focus-areas">';
      if ( $query->have_posts() ) :
        while ( $query->have_posts() ) : $query->the_post();
          $area_id       = get_the_ID();
          $area_name     = get_the_title();
          $area_image    = get_field( 'small_logo' );
          $area_color    = get_field( 'color' ); ?>

          <li>
            <?php // Assuming the Focus Areas slug never changes. ?>
            <a href="/index.php/focus-areas" class="to-focus-areas">
              <div class="content-wrap">
                <div class='image' style="border-color: <?php echo $area_color; ?>"><img src="<?php echo $area_image ?>" /></div>
                <div class="text">
                  <h3 class="area-title" style="color: <?php echo $area_color; ?>"><?php echo $area_name; ?></h3>
                </div>
              </div>
            </a>
          </li>

          <?php endwhile;
        wp_reset_postdata();
      endif;
      echo '</ul>';

      // button
      if ( $button_text && $button_link ) {
        echo '<p><a href="' . $button_link . '" class="button-cta">' . $button_text . '</a></p>';
      }
    ?>
  </div>
</section>
