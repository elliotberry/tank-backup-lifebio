<section class="columns-two section-default">
  <div class="container">
  <div class="column column-1">
      <?php the_field( 'two_columns_text_1' ); ?>
    </div>
    <div class="column column-2">
      <?php the_field( 'two_columns_text_2' ); ?>
    </div>
  </div>
</section>
