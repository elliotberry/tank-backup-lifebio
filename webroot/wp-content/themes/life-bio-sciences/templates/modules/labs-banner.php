<section class="labs-banner">
  <div class="labs-map">
    <?php echo do_shortcode( '[wpgmza id="1"]' ); ?>
  </div>
  <div class="lab-wrap">
    <div class="content">
      <?php the_content(); ?>
    </div>
    <nav class="lab-nav">
      <ul>
        <?php
          $lab_counter = 0;

          if( have_rows('labs_countries') ):
            while ( have_rows('labs_countries') ) : the_row();
              echo '<li>';
              echo get_sub_field('lab_country_name');
              echo '<ul>';
              if( have_rows('labs') ):
                while ( have_rows('labs') ) : the_row();
                  echo '<li><a href="#lab-' . $lab_counter . '">'.get_sub_field('lab_name').'</a></li>';

                  $lab_counter ++;
                endwhile;
              endif;
              echo '</ul>';
              echo '</li>';
            endwhile;
          endif;
        ?>
      </ul>
    </nav>
  </div>
</section>
<section class="labs-list">
  <?php
    $lab_counter = 0;

    if( have_rows('labs_countries') ):
      while ( have_rows('labs_countries') ) : the_row();
        echo '<div class="country-block">';
        echo '<h2 class="country-name">' . get_sub_field('lab_country_name') . '</h2>';
        echo '<div class="labs-list-wrap">';
        if( have_rows('labs') ):
          while ( have_rows('labs') ) : the_row();
            echo '<div class="lab-list-item">';
            echo '<a id="lab-' . $lab_counter . '" class="jump-anchor"></a>';
            echo '<h3 class="lab-name">' . get_sub_field('lab_name') . '</h3>';
            echo get_sub_field('lab_description');
            echo '<p>' . get_sub_field('lab_contact') . '</p>';
            echo '</div>';

            $lab_counter ++;
          endwhile;
        endif;
        echo '</div>';
        echo '</div>';
      endwhile;
    endif;
  ?>
</section>