<?php
  $hero_prefix      = ( isset( $hero_var ) ) ? $hero_var: 'hero'; // allows us to reuse the component by "passing in" a variable
  $text_color = get_field( $hero_prefix . '_text_color' );
  $text          = get_field( $hero_prefix . '_text' );
  $image         = get_field( $hero_prefix . '_image' );
  $top_image  = get_field( $hero_prefix . '_foreground_circle_thing' );
?>
<section class="hero theme-<?php echo $text_color; ?> layout-center ">
  <div class="bg-container" style="background-image: url('<?php echo $image; ?>');">

    <?php include( locate_template( 'templates/modules/hero-bubbles.php' ) ); ?>

    <div class="breadcrumbs">
      
    </div>

    <?php if ( $text ) { ?>
      <div class="container">
        <div class="content">
          <?php echo $text ?>
        </div>
      </div>
    <?php } ?>

   </div>
</section>
