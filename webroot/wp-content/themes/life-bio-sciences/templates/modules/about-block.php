<section class="about-block section-default">
  <div class="container">
    <?php the_field( 'text_block' ); ?>
  </div>
</section>
