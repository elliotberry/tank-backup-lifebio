<section class="only-on-mobile focus-area-list focus-area-list-mobile">
  <div class="container">

    <h2 class="section-title">the 8 components of aging and our companies</h2>

    <div class="section-text">
      <p>Life Biosciences is tackling all eight components of aging.</p>
      <p>Reach each hallmark to see where each of our companies is focused.</p>
    </div>

    <?php
      // focus areas
      $exclude_ids = get_field( 'exclude_focus_areas', 'option' );
      $args = array(
        'posts_per_page'   => -1,
        'post_type'    => 'lifebio_focusarea',
        'post_status'  => 'publish',
        'post__not_in' => $exclude_ids, // exclude animal companions
        'order'        => 'ASC',
        'orderby'      => 'menu_order',
      );
      $query = new WP_Query( $args );


      echo '<ul class="focus-areas with-desc">';
      if ( $query->have_posts() ) :
        while ( $query->have_posts() ) : $query->the_post();
          $area_id       = get_the_ID();
          $area_name     = get_the_title();
          $area_color    = get_field( 'color' );
          $area_logo_small = get_field( 'small_logo' );

          echo '<li>';
          echo '  <div class="content-wrap">';
          echo "    <div class='image' style='border-color: $area_color;'><img src='$area_logo_small' /></div>";
          echo '    <div class="text">';
          echo '      <h3 class="area-title" style="color:' . $area_color . '">' . $area_name . '</h3>';
          echo '    </div>';
          if (get_field( 'summary' )) {
            $desc = get_field( 'summary' );
        }
        else {
          $desc = get_the_content();
        } ?>
        <div class="desc">
<?php echo $desc; ?>
          </div>

        <?php

          echo '  </div>';
          echo '</li>';

        endwhile;
        wp_reset_postdata();
      endif;
      echo '</ul>';
    ?>
  </div>
</section>
