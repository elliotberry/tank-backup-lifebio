<section class="mission-content">
  <div class="container">
    <?php
      $title  = get_field('boxed_content_title');
      $text_1 = get_field('boxed_content_text_1');
      $text_2 = get_field('boxed_content_text_2');
    ?>
    <div class="box">
      <div class="content">
        <h2><?php echo $title; ?></h2>
      </div>
    </div>
    <div class="box">
      <div class="content">
        <?php echo $text_1; ?>
      </div>
    </div>
    <div class="box">
      <div class="content">
        <?php echo $text_2; ?>
      </div>
    </div>
    <?php
      if ( have_rows('boxed_content_boxes') ) :
        while( have_rows('boxed_content_boxes') ) : the_row();
          $style       = 'theme-light';
          $image        = get_sub_field('image');

          echo '<div class="box ' . $style . '">';
          echo '<div class="content">';
          echo '<img src="' . $image . '" alt="">';
          echo '</div>';
          echo '</div>';
        endwhile;
      endif;
    ?>
  </div>
</section>